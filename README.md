# DLCV HW02 - GAN faces

## file description
* MTCNN.py: For data pre-processing. Get rid of images which  do not contain normal faces.
* _helper.py: Some helper function for image loading and output.
* face_gen.ipynb: The main GAN model.
* output_file_rename.py: To rename the output gan results as the class requirement.
* problem_unittests.py: Unit test for each function in face_gen.ipynb
