#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from tqdm import tqdm_notebook
import os

if not os.path.exists('final_result'):
    os.makedirs('final_result')
    
for iterr in tqdm_notebook(range(500)):
    src = os.path.join("result0", str(iterr)+".png")
    dst = os.path.join("final_result", str(iterr+1).zfill(3)+"_image.png")
    os.rename(src, dst)

